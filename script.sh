#!/bin/sh

#Install dependences
echo Installing dependences
pacman -Syyu
pacman -S mariadb php php-apache apache --noconfirm

#Set shell to dash because its faster and POSIX compliant
usermod --shell /bin/dash root
usermod --shell /bin/dash mysql
usermod --shell /bin/dash http

#Setup mysql
echo Setting up mysql
mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

#Enable services
echo Enabling services
systemctl enable httpd
systemctl enable mysqld 

#Start services
echo Starting services
systemctl start httpd
systemctl start mysqld 
